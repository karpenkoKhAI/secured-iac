#TODO: Migrate to PowerShell. Links:
#https://docs.microsoft.com/en-us/rest/api/cosmos-db/create-a-document
#https://www.jbmurphy.com/2019/02/28/add-a-document-to-cosmosdb-via-the-rest-api-using-powershell/

from azure.cosmos import CosmosClient, PartitionKey, exceptions
import os

url = os.environ['ACCOUNT_URI']
key = os.environ['ACCOUNT_KEY']
client = CosmosClient(url, credential=key)


database_name  = 'configuration'
container_name = 'configuration'

database = client.get_database_client(database_name)
container = database.get_container_client(container_name)

database_client = client.get_database_client(database_name)
container_client = database.get_container_client(container_name)

container_client.upsert_item({
        'id': '1',
        'numberOfLoops': '2',
        'automaticApprovalMinutes': '4320',
        'isAutomaticApprovalEnabled': 'true'
    }
)
