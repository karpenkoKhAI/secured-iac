function detectHelmChanges {
  param (
    [parameter(Mandatory = $true,
      Position = 0)]
    [string]
    $source,

    [parameter(Mandatory = $true,
      Position = 1)]
    [string]
    $target
  )

  #Get changed files during comparing commits between feature branch and the master branch
  $changedFile = git diff --name-only $source $target
  checkLastCommandStatus -status $? -message "Failed while getting changed files"

  $changedHelmFiles = @()
  #Get changed *.tf files and tfm folders
  $changedHelmFiles = $changedFile | Select-String -Pattern 'inventory.json', '/charts' #TODO: create more safe patterns for getting changed terraform files

  $changedEnvs = @()

  if ($changedHelmFiles.Count -gt 0) {
    foreach ($file in $changedHelmFiles.Line) {
      Write-Host "Changed HELM [$file]`t file" -ForegroundColor Cyan
      if ("$(Split-Path -Path $file -Leaf)" -eq "inventory.json") {
        $changedEnvs += Split-Path $file
      }
      else {
        $changedEnvs += $file.SubString(0, $file.IndexOf("/charts/"))
      }
    }
  } 
  else {
    Write-Host "No changed any HELM files" -ForegroundColor Green
  }

  #Return changed enviroment folders
  return $changedEnvs | Get-Unique -AsString

}