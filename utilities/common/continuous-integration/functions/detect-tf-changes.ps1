function detectTfChanges {
  param (
    [parameter(Mandatory = $true,
      Position = 0)]
    [string]
    $source,

    [parameter(Mandatory = $true,
      Position = 1)]
    [string]
    $target
  )

  #Get changed files during comparing commits between feature branch and the master branch
  $changedFile = git diff --name-only $source $target
  checkLastCommandStatus -status $? -message "Failed while getting changed files"

  #Get changed *.tf files and tfm folders
  $changedTfFiles = $changedFile | Select-String -Pattern '\.tf$', '/tfm' #TODO: create more safe patterns for getting changed terraform files

  $changedEnvs = @()

  foreach ($file in $changedTfFiles.Line) {
    if ($file -Like "*.tf") {
      $changedEnvs += Split-Path $file
    }
    else {
      $changedEnvs += "$(Split-Path $file)/infra"
    }
  }

  if ($changedTfFiles.Count -gt 0) {
    foreach ($file in $changedTfFiles.Line) {
      Write-Host "Changed TF [$file]`t file" -ForegroundColor Cyan
      if ($file -Like "*.tf") {
        $changedEnvs += Split-Path $file
      }
      else {
        $changedEnvs += "$(Split-Path $file)/infra"
      }
    }
  } 
  else {
    Write-Host "No changed any TF files" -ForegroundColor Green
  }

  #Return changed enviroment folders
  return $changedEnvs | Get-Unique -AsString

}