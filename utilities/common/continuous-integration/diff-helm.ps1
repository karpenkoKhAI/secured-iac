#requires -version 7

param(
  [parameter(Mandatory = $false,
    Position = 0)]
  [string]
  $sourceBranchName = $null,

  [parameter(Mandatory = $false,
    Position = 1)]
  [string]
  $targetBranchName = $null,

  [parameter(Mandatory = $false,
    Position = 2)]
  [string]
  $envPath = $null,

  [parameter(Mandatory = $true,
    Position = 3)]
  [string]
  $projectName,

  [parameter(Mandatory = $true,
    Position = 4)]
  [string]
  $artifactoryUserName,

  [parameter(Mandatory = $true,
    Position = 5)]
  [string]
  $artifactoryUserPassword,

  [parameter(Mandatory = $false,
    Position = 6)]
  [string]
  $artifactoryBaseUrl = "https://artifactory-robotics-eu1.westeurope.cloudapp.azure.com/artifactory/helm-dev",

  [parameter(Mandatory = $true,
    Position = 7)]
  [string]
  $clientId,

  [parameter(Mandatory = $true,
    Position = 8)]
  [string]
  $clientSecret,

  [parameter(Mandatory = $false,
    Position = 9)]
  [string]
  $tenantId = "4e9dbbfb-394a-4583-8810-53f81f819e3b"
)

$ErrorActionPreference = "Stop"

#Get helpers scripts
Get-ChildItem "$PSScriptRoot/../helpers/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

#Get specific functions for CI/CD process
Get-ChildItem "$PSScriptRoot/functions/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

$changedEnvs = @()

if ($sourceBranchName -and $targetBranchName) {
  $changedEnvs = detectHelmChanges -source "origin/$sourceBranchName" -target "origin/$targetBranchName"
}
else {
  $changedEnvs += $envPath
}

Write-Host "Initializing artifactory" -ForegroundColor Cyan
helm repo add $projectName $artifactoryBaseUrl  --username $artifactoryUserName --password $artifactoryUserPassword
checkLastCommandStatus -status $? -message "Failed while add project from Artifactory"

Write-Host "Login to Azure using client [$clientId]`t and tenant [$tenantId]'t"
az login --service-principal -u $clientId -p $clientSecret -t $tenantId
checkLastCommandStatus -status $? -message "Failed while logging process to Azure"

foreach ($folder in $changedEnvs) {
  Write-Host "Detecting environment [$folder]`t application (Helm) changes" -ForegroundColor Cyan
  & "$PSScriptRoot/../deployment/deploy-service.ps1" -inventoryDir $folder -dryRun $true
}
