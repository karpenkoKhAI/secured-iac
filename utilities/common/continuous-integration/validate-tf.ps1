#requires -version 7

param(
  [parameter(Mandatory = $true,
    Position = 0)]
  [string]
  $sourceBranchName,

  [parameter(Mandatory = $true,
    Position = 1)]
  [string]
  $targetBranchName
)

$ErrorActionPreference = "Stop"

#Get helpers scripts
Get-ChildItem "$PSScriptRoot/../helpers/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

#Get specific functions for CI/CD process
Get-ChildItem "$PSScriptRoot/functions/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

$changedEnvs = detectTfChanges -source "origin/$sourceBranchName" -target "origin/$targetBranchName"

foreach ($folder in $changedEnvs) {
  Write-Host "Validation environment [$folder]" -ForegroundColor Cyan
  Push-Location $folder

  #Switching to specific terraform version
  tfenv install min-required
  tfenv use min-required

  #Download required providers without backend file
  terraform init -backend=false
  checkLastCommandStatus -status $? -message "Failed while init terrafom at [$folder]"

  terraform validate
  checkLastCommandStatus -status $? -message "Failed while validating terrafom files at [$folder]"
  # tflint --module #TODO:Enable tflit feature
  Pop-Location
}
