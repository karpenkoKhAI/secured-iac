#requires -version 7

param(
  [parameter(Mandatory = $true,
    Position = 0)]
  [string]
  $sourceBranchName,

  [parameter(Mandatory = $true,
    Position = 1)]
  [string]
  $targetBranchName,

  [parameter(Mandatory = $true,
    Position = 2)]
  [ValidateSet("checkov", "tfsec", "terrascan")]
  [string]
  $scannerName
)

$ErrorActionPreference = "Stop"

#Get helpers scripts
Get-ChildItem "$PSScriptRoot/../helpers/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

#Get specific functions for CI/CD process
Get-ChildItem "$PSScriptRoot/functions/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

$changedEnvs = detectTfChanges -source "origin/$sourceBranchName" -target "origin/$targetBranchName"

$changedEnvs

foreach ($folder in $changedEnvs) {
  Write-Host "Scanning environment [$folder]" -ForegroundColor Cyan
  Push-Location $folder

  #Switching to specific terraform version
  tfenv install min-required
  tfenv use min-required

  #Download required providers without backend file
  terraform init -backend=false
  checkLastCommandStatus -status $? -message "Failed while init terrafom at [$folder]"

  #Chossing specific scanner for IaC. Default scanner is checkov
  switch ($scannerName) {
    checkov {
      Write-Host "checkov"
    }
    tfsec {
      Write-Host "tfsec"
    }
    terrascan {
      Write-Host "terrascan"
    }
    default {
      Write-Host "checkov"
    }
  }

  Pop-Location
}
