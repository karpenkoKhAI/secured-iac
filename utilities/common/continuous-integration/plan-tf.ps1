#requires -version 7

param(
  [parameter(Mandatory = $false,
    Position = 0)]
  [string]
  $sourceBranchName = $null,

  [parameter(Mandatory = $false,
    Position = 1)]
  [string]
  $targetBranchName = $null,

  [parameter(Mandatory = $false,
    Position = 2)]
  [string]
  $envPath
)

$ErrorActionPreference = "Stop"

#Get helpers scripts
Get-ChildItem "$PSScriptRoot/../helpers/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

#Get specific functions for CI/CD process
Get-ChildItem "$PSScriptRoot/functions/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

$changedEnvs = @()

if ($sourceBranchName -and $targetBranchName) {
  $changedEnvs = detectTfChanges -source "origin/$sourceBranchName" -target "origin/$targetBranchName"
}
else {
  $changedEnvs = detectTfChanges -source HEAD~1 -target HEAD~0 | Select-String -Pattern $envPath
}

$changedEnvs

foreach ($folder in $changedEnvs) {
  Write-Host "Detecting environment [$folder]`t infrastructure changes" -ForegroundColor Cyan
  Push-Location $folder
  pwd
  #Switching to specific terraform version
  tfenv install min-required
  tfenv use min-required

  # Write-Host "Login to Azure using client [$clientId]`t and tenant [$tenantId]'t"
  # az login --service-principal -u $clientId -p $clientSecret -t $tenantId
  # checkLastCommandStatus -status $? -message "Failed while logging process to Azure"

  # terraform init
  # checkLastCommandStatus -status $? -message "Failed while init terrafom at [$folder]"

  # terraform plan -out "$(Split-Path -Path $folder -Leaf).plan" -input=false

  Pop-Location
}
