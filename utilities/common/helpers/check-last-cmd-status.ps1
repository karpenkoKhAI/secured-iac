#requires -version 7
<#
.DESCRIPTION
  This function checks the last command status
.PARAMETER status
  The last command status
.PARAMETER message
  The error message which prints in console output
.NOTES
  Version:        1.0
  Author:         andrii.karpenko@stryker.com
  Creation Date:  11-March-2021
  Purpose/Change: Initial script development
.EXAMPLE
  checkLastCommandStatus -status $? -message "Some error"
#>
function checkLastCommandStatus {
  param (
    [parameter(Mandatory = $true,
      Position = 0)]
    [bool]
    $status,

    [parameter(Mandatory = $true,
      Position = 1)]
    [string]
    $message
  )
  if (-Not $status) {
    Write-Error "$message"
  }
}