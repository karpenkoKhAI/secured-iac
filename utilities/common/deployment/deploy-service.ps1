#requires -version 7
<#
.DESCRIPTION
  This script deploys Helm charts to Kubernetes cluster.
.PARAMETER service
  The specific service to deploy/update
.PARAMETER inventoryDir
  The directory where Inventory (usually, "inventory.json") file located
.PARAMETER dryRun
  When #true, only difference between Helm Charts will be shown
.NOTES
  Version:        1.0
  Author:         andrii.karpenko@stryker.com
  Creation Date:  11-March-2021
  Purpose/Change: Initial script development

.EXAMPLE
  deployService -service 'web-portal', 'user' -inventoryDir "<enviroment_path>"
#>
param(
  [parameter(Mandatory = $false,
    Position = 0)]
  [string[]]
  $service,

  [parameter(Mandatory = $true,
    Position = 1)]
  [string]
  $inventoryDir,

  [parameter(Mandatory = $false,
    Position = 2)]
  [string]
  $inventoryFileName = 'inventory.json',

  [parameter(Mandatory = $false,
    Position = 3)]
  [bool]
  $dryRun = $false
)

$ErrorActionPreference = "Stop"

Get-ChildItem "$PSScriptRoot/../helpers/*.ps1" -Recurse | ForEach-Object { .$_ } | Out-Null

#Get service properties
try {
  $inventory = Get-Content $inventoryDir/$inventoryFileName -Raw | ConvertFrom-Json
}
catch [System.SystemException] {
  Write-Error "The Inventory file [$inventoryFileName] is empty. Please, check path to file"
}

Write-Host "Setting [$($inventory.Azure.SubscriptionName)] Azure Subscription context..." -ForegroundColor Cyan

az account set --subscription $inventory.Azure.SubscriptionName | Out-Null
checkLastCommandStatus -status $? -message "Failed while setting [$($inventory.Azure.SubscriptionName)]'t subscription"

Write-Host "Getting [$($inventory.Azure.AKSCluster)] AKS credentials..." -ForegroundColor Cyan
az aks get-credentials --resource-group $inventory.Azure.ResourceGroup --name $inventory.Azure.AKSCluster --admin
checkLastCommandStatus -status $? -message "Failed while obtaining [$($inventory.Azure.AKSCluster)] Kubernetes context in resource group [$($inventory.Azure.ResourceGroup)]"

#Get all services from Inventory file if $service variable is empty
if (-Not ($service)) {
  $service = $inventory.Services | ForEach-Object { $_.Name }
}

Write-Host "Updating Helm repository..." -ForegroundColor Cyan
helm repo update
checkLastCommandStatus -status $? -message "Failed while obtaining Helm repository"

foreach ($svc in $service) {
  foreach ($definition in $inventory.Services) {
    if ($svc -eq $definition.Name) {
      if (-Not (Test-Path "$inventoryDir/charts/$($definition.Name)")) {
        Write-Error "The path [$inventoryDir/charts/$($definition.Name)] to chart's values does not exist"
      }
      else {
        $valueFiles = Get-ChildItem -Path "$inventoryDir/charts/$($definition.Name)" -filter 'values*.yaml' -File | Join-String -Property FullName -Separator ' -f '
        if (-Not $valueFiles) {
          Write-Error "The values files for [$($definition.HelmRepoName)] does not exist"
        }
        if ($definition.HelmChartVersion -eq 'latest') {
          $chartVersion = $null
        }
        else {
          $chartVersion = "--version $($definition.HelmChartVersion)"
        }
        if ($dryRun) {
          Write-Host "Checking difference between current and deployed [$($definition.Name)] chart..." -ForegroundColor Cyan
          #Create a command template and execute this string as a command via Invoke-Expression
          (Invoke-Expression $([string]::Format("helm diff upgrade {0} {1} -f {3} {4} --namespace {2}", $definition.Name, $definition.HelmChartName, $inventory.Azure.AKSNamespace, $valueFiles, $chartVersion))) 2>&1
          checkLastCommandStatus -status $? -message "Failed while checking differences of [$($definition.Name)] service"
        }
        else {
          Write-Host "Updating [$($definition.Name)] service..." -ForegroundColor Cyan
          #Create a command template and execute this string as a command via Invoke-Expression
          (Invoke-Expression $([string]::Format("helm upgrade --install --wait {0} {2} -f {4} --set container.image.version={1} {5} --namespace {3}", $definition.Name, $definition.Version, $definition.HelmChartName, $inventory.Azure.AKSNamespace, $valueFiles, $chartVersion))) 2>&1  | Out-Null
          checkLastCommandStatus -status $? -message "Failed while updating [$($definition.Name)]"
          Write-Host "The [$($definition.Name)] service was successfully updated" -ForegroundColor Green
        }
      }
    }
  }
}
